### Manual update

Before upgrading your theme's version, you will need to request the new version at <a href="https://bindtuning.com/">BindTuning</a> and follow the steps on the <a href="https://bindtuning-sharepoint-2013-themes.readthedocs.io/en/latest/bindtuning/update/" target="_blank">next link</a>.

**Note:** **Always** make a backup of all of your theme assets, including CSS files, master pages, page layouts,etc. <br>
Upgrading the theme will remove all custom changes you have previously applied. 

___
#### Uninstall the theme

To **upgrade** your theme version, you need to first completely remove the theme from your website. 
You can find the instructions for uninstalling a theme manually or using the One-Click tool <a href="https://bindtuning-sharepoint-2013-themes.readthedocs.io/en/latest/manual/farm/uninstall/">here</a>.

___
#### Install the theme again

After completely removing the theme you can move on to installing the new version.

You can find the instructions for installing a theme manually or using the One-Click tool here: 

- <a href="https://bindtuning-sharepoint-2013-themes.readthedocs.io/en/latest/manual/farm/one-click%20installation/">One-Click Installation</a>

- <a href="https://bindtuning-sharepoint-2013-themes.readthedocs.io/en/latest/manual/farm/manual%20installation/">Manual Installation</a> 
Version upgraded! ✅

---
### One-Click update

1. Unzip the **.zip** file 
2. Open the **FarmSolution** folder;
3. Inside the **FarmSolution** folder, click twice on the **Setup.exe** file, to open the **One-click installer**;
4.  Click **Next**;

 	**Note:** The installer will now check if the theme can be upgraded. Once done, you will see a success message. If any of the requirements fail, click on **Abort**, fix the failed requirements and re-run the Installer.

6.  Click **Next**; 

7.  Select **Upgrade** and click **Next**; 

	The **Upgrade** option will only appear if the setup package has a greater version than the one currently installed.

8.  Check if your theme appears on the list and click **Next**;

	**Note:** The theme will only appear if active on your site.

9.  Your theme is now being upgraded - once done, you will see a success message! 🕐  

After the upgrade, click **Close** to exit the One-click tool.
<br>If you wish to see the installation log, click **Next**.

Version upgraded! ✅