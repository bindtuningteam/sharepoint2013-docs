Before upgrading your theme's version, you will need to request the new version at <a href="https://bindtuning.com/">BindTuning</a> and follow the steps on the <a href="https://bindtuning-sharepoint-2013-themes.readthedocs.io/en/latest/bindtuning/update" target="_blank">next link</a>.

**Note:** **Always** make a backup of all of your theme assets, including CSS files, master pages, page layouts, etc. Upgrading the theme will **remove all custom changes** previously applied. 

### Uninstall the theme

To upgrade your theme version, you need to first completely remove the theme from your website. 

You can find the instructions for uninstalling a theme 
<a href="https://bindtuning-sharepoint-2013-themes.readthedocs.io/en/latest/manual/sandbox/uninstall/" target="_blank">here</a>.

---
### Install the theme again

After completely removing the theme you can move on to installing the new version.

You can find the instructions for installing a theme <a href="https://bindtuning-sharepoint-2013-themes.readthedocs.io/en/latest/manual/sandbox/installation/" target="_blank">here</a>.

Version upgraded! ✅