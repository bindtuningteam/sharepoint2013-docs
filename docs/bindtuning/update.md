To update the BindTuning theme on your SharePoint site, you'll need to update it on your BindTuning account.

1. Login to your **BindTuning** account;
2. Select the **Design** tab;
3. On the top right of the screen click **My Themes**; 
4. If an update is available a **red circle icon** will appear on the top right corner of the selected theme. Mouse over the desired Theme and click on **More Details**;

    ![update-theme.png](..\images\update-theme.png)

    - Click the **Update Now** button to get the latest Theme files.
        
        ![updatetheme.png](..\images\updatetheme.png)


5. Click to **Download** or install using the **Provisioning engine**.

    ![download-theme.png](../images/downloadtheme.png)