After customizing and generating a theme you'll be prompted to download our **Provisioning Engine Desktop App**, that  facilitates the deployment of the selected theme. 

---
#### Download Theme files

To download the **Provisioning Engine Desktop App**: 

1. Access your BindTuning account and click the blue ribbon; <br>

    ![download-provisioning-1](../../images/download-provisioning-1.png)

**Note:** If you have already downloaded it, skip to **step 2**.

2. Go to the **My themes** tab; 
    

    - Mouse over your SharePoint 2013 theme and click on **More Details**;
    - Click on **Provisioning Engine**; 

    ![download-provisioning-2](../../images/downloadtheme.png)

    - You'll be prompted to open the **Provisioning Engine** (if already installed) or to download it. 

**Note:** The **Provisioning Engine** should be installed on your server in order for the installation process to be successful.</p>


--- 
#### Installation 

1. After opening the **Provisioning Engine**, you'll have to **Login** to your BindTuning account.

    ![provisioning-screen.png](../../images/provisioning-screen.png)
<br>
1. Set the default SharePoint version from **Office 365** to **SharePoint**; 

    ![provisioning-login](../../images/provisioning-login.png)

    - Choose your **SharePoint version**;
    - Enter your **server URL**; 
    - Enter your **Email or user** and respective **Password**.

1. Navigate to the **Install** tab;

1. Select the theme you intend to install and click on **Review & Install**;

    ![provisioning-choose-theme](../../images/provisioning-choose-theme.png)

1. Review the products to be installed and click **Install**;

1. Choose the target for the installation:

    - Classic SharePoint experience;

    ![provisioning-target-installation](../../images/provisioning-target-installation.png)

1. Review the installation, click on **Accept and proceed** and click **Install**.

1. The installation process will begin, immediately, and you'll be able to check its status, as well as to report any errors during the installation. 

You're done! ✅
